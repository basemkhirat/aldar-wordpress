<?php get_header()
?>
<div class="mainContent">
    <div class="gridContainer">
        <div class="errorPage textCentered">
            <div class="in">
                <div class="mrgBtm-md"><img src="<?php echo get_template_directory_uri().'/assets/images/404.png'?>" alt=""></div>
                <div class="number">404</div>
                <div class="font-1 fontBold mrgBtm">الصفحة غير موجودة</div>
                <div class="mrgBtm-lg">عذرا ، ولكن الصفحة التي كنت تبحث عنها لم يتم العثور عليها. حاول التحقق من URL للخطأ ، ثم اضغط على زر التحديث في المتصفح الخاص بك أو حاول العثور على شيء آخر في موقعنا .</div>
                <div class="fontBold brand-color">سيتم تحويلك في<div id="s">10</div></div>
            </div>
        </div>
    </div>
</div>
<script>
    var counter = 10;
    setInterval(function () {
        c = -- counter;
        $('#s').html(c);
    }, 1000);
    window.setTimeout(function() {
        window.location.href = '<?= site_url()?>';
    }, 10000);
</script>
<?php get_footer()?>