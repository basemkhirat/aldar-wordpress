<?php
require 'ajax_handler.php';
function resources()
{
    wp_enqueue_style('boilerplate', get_template_directory_uri() . '/assets/css/boilerplate.css');
    wp_enqueue_style('mygrid', get_template_directory_uri() . '/assets/css/mygrid.css');
    wp_enqueue_style('flexslider', get_template_directory_uri() . '/assets/css/flexslider.css');
    wp_enqueue_style('flexslider-rtl', get_template_directory_uri() . '/assets/css/flexslider-rtl.css');
    wp_enqueue_style('main', get_template_directory_uri() . '/assets/css/main.css');
    wp_enqueue_script('respond.min', get_template_directory_uri() . '/assets/js/respond.min.js');
    wp_enqueue_script('jquery', get_template_directory_uri() . '/assets/js/jquery.js');
    wp_enqueue_script('jquery.flexslider-min', get_template_directory_uri() . '/assets/js/jquery.flexslider-min.js');
    wp_enqueue_script('jquery.easing.1.3.min', get_template_directory_uri() . '/assets/js/jquery.easing.1.3.min.js');
    wp_enqueue_script('myscript', get_template_directory_uri() . '/assets/js/myscript.js');
    wp_enqueue_script('load_more', get_template_directory_uri() . '/assets/js/load_more.js');
    wp_enqueue_script('cat_load_more', get_template_directory_uri() . '/assets/js/cat_load_more.js');
    wp_enqueue_script('pload_more', get_template_directory_uri() . '/assets/js/pload_more.js');
    wp_enqueue_script('contact', get_template_directory_uri() . '/assets/js/contact-us.js');
}
add_action('wp_enqueue_scripts', 'resources');
add_theme_support('post-thumbnails');
add_theme_support('category-thumbnails');

function add_menu()
{
    register_nav_menus(array(
        'header' => 'header',
        'footer' => 'footer',
        'footer2' => 'footer2',
    ));

}
add_action('init', 'add_menu');
if ( ! isset( $content_width ) ) {
    $content_width = 600;
}
function get_post_diff($time){
    $time = (human_time_diff($time, current_time('timestamp')));
    $time = explode(' ', $time);
    $dur = $time[1];
    $time = $time[0];
    if(strpos($dur, 'hour') !== false){
        $dur = "ساعة";
    }else if (strpos($dur, 'day') !== false){
        $dur = "ايام";
    }else if (strpos($dur, 'min') !== false){
        $dur = "دقيقة";
    }else if (strpos($dur, 'week') !== false){
        $dur = "اسبوع";
    }else if (strpos($dur, 'month') !== false){
        $dur = "شهر";
    }else if (strpos($dur, 'year') !== false){
        $dur = "سنة";
    }
    $dur = array($dur, $time);
    return $dur;
}
function load_more_ajax()
{
    wp_register_script('load_more', get_stylesheet_directory_uri() . '/assets/js/load_more.js', array('jquery'));
    wp_localize_script('load_more', 'load_more', array(
        'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php',
        'current_page' => 1,
    ));

}

add_action('wp_enqueue_scripts', 'load_more_ajax');
add_action('wp_ajax_load_more', 'load_more_ajax_handler');
add_action('wp_ajax_nopriv_load_more', 'load_more_ajax_handler');

function cat_load_more_ajax(){
    wp_register_script('cat_load_more', get_stylesheet_directory_uri() . '/assets/js/cat_load_more.js', array('jquery'));
    wp_localize_script('cat_load_more', 'cat_load_more', array(
        'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php',
        'current_page' => 1,
    ));
}
add_action('wp_enqueue_scripts', 'cat_load_more_ajax');
add_action('wp_ajax_cat_load_more', 'cat_load_more_ajax_handler');
add_action('wp_ajax_nopriv_cat_load_more', 'cat_load_more_ajax_handler');

function pload_more_ajax(){
    wp_register_script('pload_more', get_stylesheet_directory_uri() . '/assets/js/pload_more.js', array('jquery'));
    wp_localize_script('pload_more', 'pload_more', array(
        'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php',
        'current_page' => 1,
    ));
}
add_action('wp_enqueue_scripts', 'pload_more_ajax');
add_action('wp_ajax_pload_more', 'pload_more_ajax_handler');
add_action('wp_ajax_nopriv_pload_more', 'pload_more_ajax_handler');

function contact_scripts()
{
    wp_register_script('contact', get_template_directory_uri() . '/assets/js/contact-us.js', array('jquery'));
    wp_localize_script('contact', 'contact_params', array(
        'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php',
    ));
}

add_action('wp_enqueue_scripts', 'contact_scripts');
add_action('wp_ajax_contact', 'contact_ajax_handler');
add_action('wp_ajax_nopriv_contact', 'contact_ajax_handler');

function setPostViews($postID) {
    $countKey = 'post_views_count';
    $count = get_post_meta($postID, $countKey, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $countKey);
        add_post_meta($postID, $countKey, '0');
    }else{
        $count++;
        update_post_meta($postID, $countKey, $count);
    }
}

function menu_has_children($id){
    $items = wp_get_nav_menu_items('header');
    $children = array();
    foreach ($items as $item){
        if($item->menu_item_parent != 0)
            $children [] = ['id' => $item->ID, 'parent' => $item->menu_item_parent, 'title' => $item->title, 'link' => $item->url];
    }
    foreach ($children as $child){
        if($id == $child['parent'])
            return true;
    }
    return false;

}
function get_menu_children($id){
    $items = wp_get_nav_menu_items('header');
    $children = array();
    foreach ($items as $item){
        if($item->menu_item_parent == $id)
            $children [] = ['id' => $item->ID, 'parent' => $item->menu_item_parent, 'title' => $item->title, 'link' => $item->url];
    }
    return $children;
}
function is_menu_child($id){
    $items = wp_get_nav_menu_items('header');
    $children = array();
    foreach ($items as $item){
        if($item->menu_item_parent != 0)
            $children [] = ['id' => $item->ID, 'parent' => $item->menu_item_parent, 'title' => $item->title, 'link' => $item->url];
    }
    foreach ($children as $child){
        if($id == $child['id'])
            return true;
    }
}

add_action("wp_ajax_news_letter", "news_letter_function");
add_action("wp_ajax_nopriv_news_letter", "news_letter_function");

function news_letter_function() {


    global $wpdb;

    $email = $email = $_POST[ 'email' ];
    $arr = [];
    $index = 0;

    $date = new DateTime();
    $created_at = $date->getTimestamp();


    if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
        $errors[] = 'Invalid email format!';

    }else{

        $table_name = $wpdb->prefix . "wysija_user";
        $test = $wpdb->insert($table_name, array('email' => $email , 'created_at' => $created_at, 'status'=> 1) );

        $sql = "INSERT INTO 'wp_wysija_user' ('email', 'created_at', 'status') VALUES ($email,  '0',  '1')";

        if($test == false){
            $errors[] = 'You are already subscribed!';
        }
    }

    if(!empty($errors)){

        foreach ($errors as $key => $all) {
            $arr[$index]['error'] = $all;
        }

    }else{
        $arr[$index]['success'] = 'تم الاشترلك بنجاح';
    }


    header('Content-Type: application/json');
    $text = json_encode($email);
    die(json_encode($arr)) ;

}

add_theme_support( 'post-formats', array( 'video', 'gallery' ) );

