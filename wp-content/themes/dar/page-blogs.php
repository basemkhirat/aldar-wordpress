<?php get_header();
$section = get_post(9)->post_title;
$section_name = get_post(9)->post_name;
$args = array('post_type' => $section_name, 'posts_per_page' => 15);
$posts = new WP_Query($args);
if($posts->have_posts()){
    $x = 1;
    ?>
<div class="mainContent">
    <div class="gridContainer">
        <div class="myrow clearfix">

            <div class="mycol-lg-9">
                <div class="myrow clearfix">
                    <?php while($posts->have_posts()){
                        $posts->the_post();
                        $time = get_post_diff(get_post_time("U", true, get_the_ID()));
                        $dur = $time[0];
                        $time = $time[1];
                        $tag = null;
                        if(has_tag())
                        {
                            $tag = get_the_tags()[0]->name;
                            $tag_url = get_tag_link(get_the_tags()[0]->term_id);
                        }
                    ?>
                    <div class="mycol-xmd-4 mycol-sm-6">
                        <div class="mainPost articlePost mrgBtm">

                            <div class="theWriter">
                                <?php echo get_avatar(get_the_author_meta( 'ID' ))?>
                                <div class="info">
                                    <div class="name"><a href="#"><?php echo get_author_name()?></a></div>
                                    <div><?php echo get_field('user_type')?></div>
                                </div>
                            </div>
                            <?php if($x % 2 != 0){
                                ?><div class="avatar"><img src="<?php echo get_the_post_thumbnail_url()?>" alt=""></div>
                            <?php }?>
                            <div class="content">
                                <div class="titleArea">
                                    <div class="title"><a href="<?php echo get_permalink()?>"><?php echo get_the_title()?></a></div>
                                    <div class="time"><?php echo "منذ ".$time." ".$dur?> </div>
                                </div>
                                <?php if($x % 2 == 0){
                                    ?>
                                    <div class="description">
                                        <div class="in">
                                            <p><?php echo get_the_excerpt()?></p>
                                        </div>
                                    </div>
                                <?php }?>

                                <div class="shareAndSection clearfix">
                                    <a href="<?= $tag_url?>" class="pullRight"><?php if($tag != null) echo $tag?></a>
                                    <div class="mainSocial pullLeft">
                                        <a href="<?php echo get_permalink()."?share=true&sharer=twitter&id".get_the_ID()?>"><i class="icon-twitter"></i></a>
                                        <a href="<?php echo get_permalink()."?share=true&sharer=facebook&id=".get_the_ID()?>"><i class="icon-facebook-official"></i></a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                <?php $x++;}?>
                    <div class="add" style="display: none">
                    </div>
                    <div class="more" style="display: none">
                        <input name="post_type" id="post_type" value="<?php echo $section_name?>">
                        <input name="name" id="name" value="<?php echo $section?>">
                        <input name="ppp" id="ppp" value="15">
                        <input name="cat_id" id="cat_id" value="0">
                    </div>
                </div>
               <div id="load_more" style="cursor: pointer"> <a class="mainBtn">المزيد</a></div>
            </div>
            <div class="mycol-lg-3 ipadProHide">

                <div class="oneWidget markets mrgBtm ">

                    <div class="mainTitle">
                        <div class="name">الاسواق</div>
                    </div>
                    <hr>
                    <div class="h-padding">
                        <div class="tabs">
                            <a href="#">DFMGI</a>
                            <a href="#" class="active">ADI</a>
                        </div>
                        <div>
                            <div class="font-4 v-padding ltrDir clearfix">
                                <span class="pullLeft">4,519.58</span>
                                <span class="pullRight brand-color">-0.09% -4</span>
                            </div>
                            <div class="graph"></div>
                            <div class="font-medium tableDis tradingNums">
                                <div class="oneCell">
                                    <div>حجم التداول</div>
                                    <div class="brand-color">381,870,534</div>
                                </div>
                                <div class="oneCell">
                                    <div>قيمة التداول</div>
                                    <div class="brand-color">381,870,534</div>
                                </div>
                                <div class="oneCell">
                                    <div>عدد العمليات</div>
                                    <div class="brand-color">381,870,534</div>
                                </div>
                            </div>
                            <div class="dataBar clearfix">
                                <div class="one" style="width: 40%;">
                                    <div>20</div>
                                    <div class="thebar"></div>
                                    <div>رابحه</div>
                                </div>
                                <div class="one" style="width: 20%;">
                                    <div>10</div>
                                    <div class="thebar"></div>
                                    <div>ثابته</div>
                                </div>
                                <div class="one" style="width: 40%;">
                                    <div>20</div>
                                    <div class="thebar"></div>
                                    <div>خاسره</div>
                                </div>
                            </div>
                            <div class="brand-bg smallPadding textCentered white-color">الشركات المتداوله <span>83</span></div>
                            <div class="mainPadding brand-color textCentered font-2 fontBold">الشركات الاكثر</div>
                            <div class="tabs tabs2">
                                <a href="#" class="active">ربحًا</a>
                                <a href="#">خسارة</a>
                                <a href="#">قيمة</a>
                                <a href="#">كمية</a>
                            </div>
                            <div class="allCompanies">
                                <div class="one tableDis">
                                    <div class="oneCell">اسمنت الاتحاد</div>
                                    <div class="oneCell">20</div>
                                    <div class="oneCell"><i class="icon-up-dir"></i></div>
                                </div>
                                <div class="one tableDis">
                                    <div class="oneCell">اسمنت الاتحاد</div>
                                    <div class="oneCell">20</div>
                                    <div class="oneCell"><i class="icon-up-dir"></i></div>
                                </div>
                                <div class="one tableDis">
                                    <div class="oneCell">اسمنت الاتحاد</div>
                                    <div class="oneCell">20</div>
                                    <div class="oneCell"><i class="icon-up-dir"></i></div>
                                </div>
                                <div class="one tableDis">
                                    <div class="oneCell">اسمنت الاتحاد</div>
                                    <div class="oneCell">20</div>
                                    <div class="oneCell"><i class="icon-up-dir"></i></div>
                                </div><div class="one tableDis">
                                    <div class="oneCell">اسمنت الاتحاد</div>
                                    <div class="oneCell">20</div>
                                    <div class="oneCell"><i class="icon-up-dir"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="oneWidget weather mrgBtm">
                    <div class="mainTitle">
                        <div class="name">الطقس</div>
                    </div>
                    <hr>
                    <div class="mainPadding">
                        <div class="tableDis">
                            <div class="oneCell"><img src="images/widgets/weather.png" alt=""></div>
                            <div class="oneCell brand-color textCentered dataTop">
                                <div>المغرب</div>
                                <div class="number"><div class="inlineBlock relative">26 <span>o</span></div></div>
                                <div class="font-small">غالبا مشمش</div>
                            </div>
                            <div class="oneCell brand-color textCentered dataTop">
                                <div>الخميس</div>
                                <div class="number">15</div>
                                <div class="font-small">مارس ٢٠١٦</div>
                            </div>
                        </div>
                    </div>
                    <div class="brand-bg hourlyWeater">
                        <div class="flexslider arrows1 downPosition">
                            <ul class="slides">
                                <li>
                                    <div class="one">
                                        <div class="avatar"><img src="images/widgets/weather2.png" alt=""></div>
                                        <div class="data">
                                            <div class="time">9 AM</div>
                                            <div class="number"><div class="inlineBlock relative">26 <span>o</span></div></div>
                                        </div>
                                    </div>

                                </li>
                                <li>
                                    <div class="one">
                                        <div class="avatar"><img src="images/widgets/weather2.png" alt=""></div>
                                        <div class="data">
                                            <div class="time">10 AM</div>
                                            <div class="number"><div class="inlineBlock relative">26 <span>o</span></div></div>
                                        </div>
                                    </div>

                                </li>
                                <li>
                                    <div class="one">
                                        <div class="avatar"><img src="images/widgets/weather2.png" alt=""></div>
                                        <div class="data">
                                            <div class="time">11 AM</div>
                                            <div class="number"><div class="inlineBlock relative">26 <span>o</span></div></div>
                                        </div>
                                    </div>

                                </li>
                                <li>
                                    <div class="one">
                                        <div class="avatar"><img src="images/widgets/weather2.png" alt=""></div>
                                        <div class="data">
                                            <div class="time">12 PM</div>
                                            <div class="number"><div class="inlineBlock relative">26 <span>o</span></div></div>
                                        </div>
                                    </div>

                                </li>
                                <li>
                                    <div class="one">
                                        <div class="avatar"><img src="images/widgets/weather2.png" alt=""></div>
                                        <div class="data">
                                            <div class="time">1 PM</div>
                                            <div class="number"><div class="inlineBlock relative">26 <span>o</span></div></div>
                                        </div>
                                    </div>

                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="content">
                        <div class="one mainPadding">
                            <div class="tableDis">
                                <div class="oneCell">الخميس</div>
                                <div class="oneCell"><img src="images/widgets/weather.png" alt=""></div>
                                <div class="oneCell number"><div class="inlineBlock relative">26 <span>o</span></div></div>
                                <div class="oneCell number"><div class="inlineBlock relative">16 <span>o</span></div></div>
                            </div>
                        </div>
                        <div class="one mainPadding">
                            <div class="tableDis">
                                <div class="oneCell">الجمعه</div>
                                <div class="oneCell"><img src="images/widgets/weather.png" alt=""></div>
                                <div class="oneCell number"><div class="inlineBlock relative">26 <span>o</span></div></div>
                                <div class="oneCell number"><div class="inlineBlock relative">16 <span>o</span></div></div>
                            </div>
                        </div>
                        <div class="one mainPadding">
                            <div class="tableDis">
                                <div class="oneCell">السبت</div>
                                <div class="oneCell"><img src="images/widgets/weather.png" alt=""></div>
                                <div class="oneCell number"><div class="inlineBlock relative">26 <span>o</span></div></div>
                                <div class="oneCell number"><div class="inlineBlock relative">16 <span>o</span></div></div>
                            </div>
                        </div>
                        <div class="one mainPadding">
                            <div class="tableDis">
                                <div class="oneCell">الاحد</div>
                                <div class="oneCell"><img src="images/widgets/weather.png" alt=""></div>
                                <div class="oneCell number"><div class="inlineBlock relative">26 <span>o</span></div></div>
                                <div class="oneCell number"><div class="inlineBlock relative">16 <span>o</span></div></div>
                            </div>
                        </div>
                        <div class="one mainPadding">
                            <div class="tableDis">
                                <div class="oneCell">الاتنين</div>
                                <div class="oneCell"><img src="images/widgets/weather.png" alt=""></div>
                                <div class="oneCell number"><div class="inlineBlock relative">26 <span>o</span></div></div>
                                <div class="oneCell number"><div class="inlineBlock relative">16 <span>o</span></div></div>
                            </div>
                        </div>
                        <div class="one mainPadding">
                            <div class="tableDis">
                                <div class="oneCell">الثلاثاء</div>
                                <div class="oneCell"><img src="images/widgets/weather.png" alt=""></div>
                                <div class="oneCell number"><div class="inlineBlock relative">26 <span>o</span></div></div>
                                <div class="oneCell number"><div class="inlineBlock relative">16 <span>o</span></div></div>
                            </div>
                        </div>
                        <div class="one mainPadding">
                            <div class="tableDis">
                                <div class="oneCell">الاربعاء</div>
                                <div class="oneCell"><img src="images/widgets/weather.png" alt=""></div>
                                <div class="oneCell number"><div class="inlineBlock relative">26 <span>o</span></div></div>
                                <div class="oneCell number"><div class="inlineBlock relative">16 <span>o</span></div></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="oneWidget fuelPrice mrgBtm">
                    <div class="mainTitle">
                        <div class="name">أسعار الوقود</div>
                    </div>
                    <hr>
                    <div class="content mainPadding">
                        <div class="one">
                            <div class="tableDis">
                                <div class="oneCell"><img src="images/widgets/gas.png" alt=""></div>
                                <div class="oneCell"><span class="font-4 brand-color">جازولين</span></div>
                                <div class="oneCell">سوبر 98 <br> 2.5</div>
                            </div>
                        </div>
                        <div class="one">
                            <div class="tableDis">
                                <div class="oneCell"><img src="images/widgets/gas.png" alt=""></div>
                                <div class="oneCell"><span class="font-4 brand-color">جازولين</span></div>
                                <div class="oneCell">سوبر 95 <br> 2.3</div>
                            </div>
                        </div>
                        <div class="one">
                            <div class="tableDis">
                                <div class="oneCell"><img src="images/widgets/gas.png" alt=""></div>
                                <div class="oneCell"><span class="font-4 brand-color">جازولين</span></div>
                                <div class="oneCell">بلس <br> 2.0</div>
                            </div>
                        </div>
                        <div class="one">
                            <div class="tableDis">
                                <div class="oneCell"><img src="images/widgets/gas.png" alt=""></div>
                                <div class="oneCell"><span class="font-4 brand-color">ديزل</span></div>
                                <div class="oneCell">2.0</div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
<?php }get_footer()?>