<?php
$args = array(
    'post_type' => 'news',
    'cat' => 14,
    //'posts_per_page' => 6
);
$cat = get_cat_name(14);
$news = new WP_Query($args);
if($news->have_posts()){
    ?>
<div class="mrgBtm">
    <div class="mainTitle"><h2 class="name"><?php echo $cat?></h2></div>
    <div class="flexslider normalSlider arrows1 downPosition investigationsSlider ">
        <ul class="slides">
            <?php while ($news->have_posts()){
                $news->the_post();
            ?>
            <li>
                <div class="one">
                    <a href="<?php echo get_permalink()?>" class="avatar"><img src="<?php echo get_the_post_thumbnail_url()?>" class="bgCover" alt=""></a>
                </div>
            </li>
        <?php }?>
        </ul>
    </div>
</div>
<?php }
$args = array(
    'post_type' => 'news',
    'cat' => 15,
    //'posts_per_page' => 6
);
$cat = get_cat_name(15);
$news = new WP_Query($args);
if($news->have_posts()){
    $x = 0;
?>

<div class="worldsNews mrgBtm">
    <div class="mainTitle"><h2 class="name"><?php echo $cat?></h2></div>
    <?php while ($news->have_posts()){
        $news->the_post();
        $tag = null;
        if(has_tag())
            $tag = get_the_tags()[0]->name;
        $time = get_post_diff(get_post_time("U", true, get_the_ID()));
        $dur = $time[0];
        $time = $time[1];
        ?>
    <div class="mainPost">
        <?php if($x == 0){?><div class="avatar"><img src="<?php echo get_the_post_thumbnail_url()?>" alt=""></div><?php }?>
        <div class="content">
            <div class="titleArea">
                <div class="title"><a href="<?php echo get_permalink()?>"><?php echo get_the_title()?> </a></div>
                <div class="time"><?php echo "منذ ".$time." ".$dur?></div>
            </div>
            <div class="shareAndSection clearfix">
                <a href="#" class="pullRight"><?php if($tag != null) echo $tag?></a>
                <div class="mainSocial pullLeft">
                    <a href="<?php echo get_permalink()."?share=true&sharer=twitter&id".get_the_ID()?>"><i class="icon-twitter"></i></a>
                    <a href="<?php echo get_permalink()."?share=true&sharer=facebook&id=".get_the_ID()?>"><i class="icon-facebook-official"></i></a>

                </div>
            </div>
        </div>

    </div>
        <?php $x = 1; }?>

</div>
<?php }?>