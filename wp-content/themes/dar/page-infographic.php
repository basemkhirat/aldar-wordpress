<?php
get_header();
$section = get_post(206)->post_title;
$section_name = get_post(206)->post_name;
$args = array('post_type' => $section_name, 'posts_per_page' => 9);
$posts = new WP_Query($args);
$url = get_permalink(261);

if($posts->have_posts()) {
    $posts->the_post();
    ?>
    <div class="mainContent" >
		<div class="gridContainer" >
			<div class="myrow clearfix" >

				<div class="mycol-lg-6 mycol-xmd-8" >
					<div class="caricaturePost main mrgBtm" >
                        <a class="avatar openPopup" style="pointer-events: none;"><img src = "<?php echo get_the_post_thumbnail_url()?>" class="bgCover" alt = "" ></a>
						<div class="mainSocial" >
                            <a href="<?php echo get_permalink()."?share=true&sharer=twitter&id".get_the_ID()?>"><i class="icon-twitter"></i></a>
                            <a href="<?php echo get_permalink()."?share=true&sharer=facebook&id=".get_the_ID()?>"><i class="icon-facebook-official"></i></a>
						</div >
					</div >
				</div >

                <?php while($posts->have_posts()){ $posts->the_post();?>
				<div class="mycol-lg-3 mycol-xmd-4 mycol-sm-6" >
					<div class="caricaturePost mrgBtm" >
                        <a class="avatar " style="pointer-events: none;"><img src = "<?php echo get_the_post_thumbnail_url()?>" class="bgCover " alt = "" ></a>
					</div >
				</div >
        <?php }?>
                <div class="add" style="display: none">
                    <input name="post_type" id="post_type" value="<?php echo $section_name?>">
                    <input name="name" id="name" value="<?php echo $section?>">
                    <input name="ppp" id="ppp" value="12">
                </div>
			</div >
            <div id="load_more" style="cursor: pointer"> <a class="mainBtn" > المزيد</a ></div>
		</div >
	</div >
    <?php }
    get_footer()?>