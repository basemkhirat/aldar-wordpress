<?php
get_header();
$section = get_post(8)->post_title;
$section_name = get_post(8)->post_name;
$args = array('post_type' => $section_name, 'posts_per_page' => 11);
$posts = new WP_Query($args);
$url = get_permalink(152);
if($posts->have_posts()){
    $x = 0;
?>
    <div class="mainContent">
<div class="white-bg mrgBtm">
    <div class="gridContainer">

        <div class="videosSlider">
            <div id="slider" class="flexslider">
                <ul class="slides">
                    <?php while($posts->have_posts()){
                        if($x == 3) break;
                        $posts->the_post();
                        ?>
                        <li data-thumb="<?php echo get_the_post_thumbnail_url()?>">
                            <div class="videoSliderPost">
                                <div thumb="<?php echo get_the_post_thumbnail_url()?>"
                                     content="<?php echo get_the_content()?>"
                                     title="<?php echo get_the_title()?>"
                                     id="<?php echo get_the_ID()?>"
                                     class="avatar openPopup" style="cursor: pointer">
                                    <img src="<?php echo get_the_post_thumbnail_url()?>" class="bgCover" alt=""></div>
                                <div class="content">
                                    <a class="title clearfix">
                                        <i class="icon-right-dir"></i>
                                        <span>
                                        <?php echo get_the_title()?>
                                    </span>
                                    </a>
                                </div>
                            </div>
                        </li>
                    <?php $x++;}?>
                </ul>
            </div>
        </div>


    </div>
</div>

<div class="gridContainer">
    <div class="myrow clearfix">
        <?php while ($posts->have_posts()){

            $posts->the_post();
            $time = get_post_diff(get_post_time("U", true, get_the_ID()));
            $dur = $time[0];
            $time = $time[1];
            ?>
        <div class="mycol-lg-3 mycol-xmd-4 mycol-sm-6">
            <div class="videoPost mrgBtm">
                <div thumb="<?php echo get_the_post_thumbnail_url()?>"
                     content="<?php echo get_the_content()?>"
                     title="<?php echo get_the_title()?>"
                     id="<?php echo get_the_ID()?>"
                     class="avatar openPopup" style="cursor: pointer">
                    <img src="<?php echo get_the_post_thumbnail_url()?>" class="bgCover" alt=""></div>
                <div class="content">
                    <div class="title">
                        <a  style="cursor: pointer"
                            thumb="<?php echo get_the_post_thumbnail_url()?>"
                            content="<?php echo get_the_content()?>"
                            title="<?php echo get_the_title()?>"
                            id="<?php echo get_the_ID()?>" class="openPopup">
                            <?php echo get_the_title()?>
                        </a>
                    </div>
                    <div><?php echo "منذ ".$time." ".$dur?></div>
                </div>
            </div>
        </div>
        <?php }?>
        <div class="add" style="display: none">
            <input name="post_type" id="post_type" value="<?php echo $section_name?>">
            <input name="name" id="name" value="<?php echo $section?>">
            <input name="ppp" id="ppp" value="12">
        </div>
    </div>
    <div id="load_more" style="cursor: pointer"> <a class="mainBtn" > المزيد</a ></div>
</div>
    </div>
<?php }get_footer()?>