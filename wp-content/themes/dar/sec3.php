<div class="gridContainer">
    <div class="myrow clearfix">

        <?php
        $args = array(
            'post_type' => 'news',
            'cat' => 8,
            'posts_per_page' => 6
        );
        $cat = get_cat_name(8);
        $news = new WP_Query($args);
        if($news->have_posts()){
        $x = 0;
        ?>
        <div class="mycol-lg-3 mycol-sm-6">
            <div class="worldsNews mrgBtm">
                <div class="mainTitle"><h2 class="name"><?php echo $cat?></h2></div>
                <?php while ($news->have_posts()){
                    $news->the_post();
                    $tag = null;
                    if(has_tag()) {
                        $tag = get_the_tags()[0]->name;
                        $tag_url = get_tag_link(get_the_tags()[0]->term_id);
                    }
                    $time = get_post_diff(get_post_time("U", true, get_the_ID()));
                    $dur = $time[0];
                    $time = $time[1];
                    ?>
                    <div class="mainPost">
                        <?php if($x == 0){?><div class="avatar"><img src="<?php echo get_the_post_thumbnail_url()?>" alt=""></div><?php }?>
                        <div class="content">
                            <div class="titleArea">
                                <div class="title"><a href="<?php echo get_permalink()?>"><?php echo get_the_title()?> </a></div>
                                <div class="time"><?php echo "منذ ".$time." ".$dur?></div>
                            </div>
                            <div class="shareAndSection clearfix">
                                <a href="<?= $tag_url?>" class="pullRight"><?php if($tag != null) echo $tag?></a>
                                <div class="mainSocial pullLeft">
                                    <a href="<?php echo get_permalink()."?share=true&sharer=twitter&id".get_the_ID()?>"><i class="icon-twitter"></i></a>
                                    <a href="<?php echo get_permalink()."?share=true&sharer=facebook&id=".get_the_ID()?>"><i class="icon-facebook-official"></i></a>

                                </div>
                            </div>
                        </div>

                    </div>
                    <?php $x = 1; }?>

            </div>
            <?php }?>

        </div>
        <?php
        $args = array(
            'post_type' => 'news',
            'cat' => 9,
            'posts_per_page' => 6
        );
        $cat = get_cat_name(9);
        $news = new WP_Query($args);
        if($news->have_posts()){
        $x = 0;
        ?>
        <div class="mycol-lg-3 mycol-sm-6">
            <div class="worldsNews mrgBtm">
                <div class="mainTitle"><h2 class="name"><?php echo $cat?></h2></div>
                <?php while ($news->have_posts()){
                    $news->the_post();
                    $tag = null;
                    if(has_tag()) {
                        $tag = get_the_tags()[0]->name;
                        $tag_url = get_tag_link(get_the_tags()[0]->term_id);
                    }
                    $time = get_post_diff(get_post_time("U", true, get_the_ID()));
                    $dur = $time[0];
                    $time = $time[1];
                    ?>
                    <div class="mainPost">
                        <?php if($x == 0){?><div class="avatar"><img src="<?php echo get_the_post_thumbnail_url()?>" alt=""></div><?php }?>
                        <div class="content">
                            <div class="titleArea">
                                <div class="title"><a href="<?php echo get_permalink()?>"><?php echo get_the_title()?> </a></div>
                                <div class="time"><?php echo "منذ ".$time." ".$dur?></div>
                            </div>
                            <div class="shareAndSection clearfix">
                                <a href="<?= $tag_url?>" class="pullRight"><?php if($tag != null) echo $tag?></a>
                                <div class="mainSocial pullLeft">
                                    <a href="<?php echo get_permalink()."?share=true&sharer=twitter&id".get_the_ID()?>"><i class="icon-twitter"></i></a>
                                    <a href="<?php echo get_permalink()."?share=true&sharer=facebook&id=".get_the_ID()?>"><i class="icon-facebook-official"></i></a>

                                </div>
                            </div>
                        </div>

                    </div>
                    <?php $x = 1; }?>

            </div>
            <?php }?>

        </div>
        <?php
        $args = array(
            'post_type' => 'news',
            'cat' => 10,
            'posts_per_page' => 6
        );
        $cat = get_cat_name(10);
        $news = new WP_Query($args);
        if($news->have_posts()){
        $x = 0;
        ?>
        <div class="mycol-lg-3 mycol-sm-6">
            <div class="worldsNews mrgBtm">
                <div class="mainTitle"><h2 class="name"><?php echo $cat?></h2></div>
                <?php while ($news->have_posts()){
                    $news->the_post();
                    $tag = null;
                    if(has_tag()) {
                        $tag = get_the_tags()[0]->name;
                        $tag_url = get_tag_link(get_the_tags()[0]->term_id);
                    }
                    $time = get_post_diff(get_post_time("U", true, get_the_ID()));
                    $dur = $time[0];
                    $time = $time[1];
                    ?>
                    <div class="mainPost">
                        <?php if($x == 0){?><div class="avatar"><img src="<?php echo get_the_post_thumbnail_url()?>" alt=""></div><?php }?>
                        <div class="content">
                            <div class="titleArea">
                                <div class="title"><a href="<?php echo get_permalink()?>"><?php echo get_the_title()?> </a></div>
                                <div class="time"><?php echo "منذ ".$time." ".$dur?></div>
                            </div>
                            <div class="shareAndSection clearfix">
                                <a href="<?= $tag_url?>" class="pullRight"><?php if($tag != null) echo $tag?></a>
                                <div class="mainSocial pullLeft">
                                    <a href="<?php echo get_permalink()."?share=true&sharer=twitter&id".get_the_ID()?>"><i class="icon-twitter"></i></a>
                                    <a href="<?php echo get_permalink()."?share=true&sharer=facebook&id=".get_the_ID()?>"><i class="icon-facebook-official"></i></a>

                                </div>
                            </div>
                        </div>

                    </div>
                    <?php $x = 1; }?>

            </div>
            <?php }?>

        </div>
        <?php
        $args = array(
            'post_type' => 'news',
            'cat' => 11,
            'posts_per_page' => 6
        );
        $cat = get_cat_name(11);
        $news = new WP_Query($args);
        if($news->have_posts()){
        $x = 0;
        ?>
        <div class="mycol-lg-3 mycol-sm-6">
            <div class="worldsNews mrgBtm">
                <div class="mainTitle"><h2 class="name"><?php echo $cat?></h2></div>
                <?php while ($news->have_posts()){
                    $news->the_post();
                    $tag = null;
                    if(has_tag()) {
                        $tag = get_the_tags()[0]->name;
                        $tag_url = get_tag_link(get_the_tags()[0]->term_id);
                    }
                    $time = get_post_diff(get_post_time("U", true, get_the_ID()));
                    $dur = $time[0];
                    $time = $time[1];
                    ?>
                    <div class="mainPost">
                        <?php if($x == 0){?><div class="avatar"><img src="<?php echo get_the_post_thumbnail_url()?>" alt=""></div><?php }?>
                        <div class="content">
                            <div class="titleArea">
                                <div class="title"><a href="<?php echo get_permalink()?>"><?php echo get_the_title()?> </a></div>
                                <div class="time"><?php echo "منذ ".$time." ".$dur?></div>
                            </div>
                            <div class="shareAndSection clearfix">
                                <a href="<?= $tag_url?>" class="pullRight"><?php if($tag != null) echo $tag?></a>
                                <div class="mainSocial pullLeft">
                                    <a href="<?php echo get_permalink()."?share=true&sharer=twitter&id".get_the_ID()?>"><i class="icon-twitter"></i></a>
                                    <a href="<?php echo get_permalink()."?share=true&sharer=facebook&id=".get_the_ID()?>"><i class="icon-facebook-official"></i></a>

                                </div>
                            </div>
                        </div>

                    </div>
                    <?php $x = 1; }?>

            </div>
            <?php }?>

        </div>
</div>
</div>