<?php

function load_more_ajax_handler(){
    $page = $_POST['page']+1;
    $post_type = $_POST['post_type'];
    $ppp = $_POST['ppp'];
    $args = array(
        'post_type'      => $post_type,
        'posts_per_page' => $ppp,
        'paged' => $page
    );
    $posts = new WP_Query($args);

    $html = "";
    $x = 1;
    $response = array();
    while($posts->have_posts()){
        $posts->the_post();
        $time = get_post_diff(get_post_time("U", true, get_the_ID()));
        $dur = $time[0];
        $time = $time[1];
        $tag = null;
        if(has_tag())
        {
            $tag = get_the_tags()[0]->name;
            $tag_url = get_tag_link(get_the_tags()[0]->term_id);
        }
        if($post_type == "blog") {
            $html .= "   <div class=\"mycol-xmd-4 mycol-sm-6\">
                        <div class=\"mainPost articlePost mrgBtm\">

                            <div class=\"theWriter\">
                                " . get_avatar(get_the_author_meta('ID')) . "
                                <div class=\"info\">
                                    <div class=\"name\"><a href=\"#\">" . get_author_name() . "</a></div>
                                    <div>" . get_field('user_type') . "</div>
                                </div>
                            </div>";
            if ($x % 2 != 0) {
                $html .= " <div class=\"avatar\"><img src=\"" . get_the_post_thumbnail_url() . "\" alt=\"\"></div>";
            }
            $html .= "<div class=\"content\">
                                <div class=\"titleArea\">
                                    <div class=\"title\"><a href=\"" . get_permalink() . "\">" . get_the_title() . "</a></div>
                                    <div class=\"time\">منذ " . $time . " " . $dur . "</div>
                                </div>";
            if ($x % 2 == 0) {
                $html .=
                    "<div class=\"description\">
                                        <div class=\"in\">
                                            <p>" . get_the_content() . "</p>
                                        </div>
                                    </div>";
            }

            $html .= "<div class=\"shareAndSection clearfix\">
                                    <a href=\"" . $tag_url . "\" class=\"pullRight\">";
            if ($tag != null) $html .= $tag . "</a>";
            $html .= "<div class=\"mainSocial pullLeft\">
                                        <a href=\"".get_permalink()."?share=true&sharer=twitter&id\"".get_the_ID()."\"><i class=\"icon-twitter\"></i></a>
												<a href=\"".get_permalink()."?share=true&sharer=facebook&id=\"".get_the_ID()."\"><i class=\"icon-facebook-official\"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                   </div>";
        }else if($post_type == "infographic"){
            $html.= "<div class=\"mycol-lg-3 mycol-xmd-4 mycol-sm-6\" >
					<div class=\"caricaturePost mrgBtm\" >
                        <a class=\"avatar \" style=\"pointer-events: none;\"><img src = \"".get_the_post_thumbnail_url()."\" class=\"bgCover fullWidth\" alt = \"\" ></a>
					</div >
				</div >";
        }else if($post_type == "video"){
            $html .= "<div class=\"mycol-lg-3 mycol-xmd-4 mycol-sm-6\">
            <div class=\"videoPost mrgBtm\">
                <div thumb=\"".get_the_post_thumbnail_url()."\"
                            content=\"".get_the_content()."\"
                            title=\"".get_the_title()."\"
                            id=\"".get_the_ID()."\" class=\"avatar openPopup\" style=\"cursor: pointer\">
                    <img src=\"".get_the_post_thumbnail_url()."\" class=\"bgCover fullWidth\" alt=\"\"></div>
                <div class=\"content\">
                    <div class=\"title\">
                        <a  style=\"cursor: pointer\"
                            thumb=\"".get_the_post_thumbnail_url()."\"
                            content=\"".get_the_content()."\"
                            title=\"".get_the_title()."\"
                            id=\"".get_the_ID()."\" class=\"openPopup\">
                            ".get_the_title()."
                        </a>
                    </div>
                    <div>منذ $time $dur</div>
                </div>
            </div>
        </div>";
        }
        $x++;

    }
    $response['html'] = $html;
    $response['no_more'] = false;
    if($page < $posts->max_num_pages)
        $response['no_more'] = true;
    wp_send_json($response);
}
function cat_load_more_ajax_handler(){

    $page = $_POST['page']+1;
    $cat_id = $_POST['cat_id'];
    $args = array(
       'posts_per_page' => 6,
        'paged' => $page
    );
    if($_SESSION['type'] == 'tag'){
        $args['post_type'] = array('news', 'main-slider', 'blog');
        $args['tag__in'] = $cat_id;
    }else{
        $args['post_type'] = 'news';
        $args['cat'] = $cat_id;
    }

    $posts = new WP_Query($args);
    $html = "";
    $x = 1;
    $response = array();
    while($posts->have_posts()){
        $posts->the_post();
        $time = get_post_diff(get_post_time("U", true, get_the_ID()));
        $dur = $time[0];
        $time = $time[1];
        $tag = null;
        if(has_tag())
        {
            $tag = get_the_tags()[0]->name;
            $tag_url = get_tag_link(get_the_tags()[0]->term_id);
        }
            $html .= "   <div class=\"mycol-xmd-4 mycol-sm-6\"><div class=\"mainPost  mrgBtm\">";
            if ($x % 2 != 0) {
                $html .= " <div class=\"avatar\"><img src=\"" . get_the_post_thumbnail_url() . "\" alt=\"\"></div>";
            }
            $html .= "<div class=\"content\">
                                <div class=\"titleArea\">
                                    <div class=\"title\"><a href=\"" . get_permalink() . "\">" . get_the_title() . "</a></div>
                                    <div class=\"time\">منذ " . $time . " " . $dur . "</div>
                                </div>";
            if ($x % 2 == 0) {
                $html .=
                    "<div class=\"description\">
                                        <div class=\"in\">
                                            <p>" . get_the_content() . "</p>
                                        </div>
                                    </div>";
            }

            $html .= "<div class=\"shareAndSection clearfix\">
                                    <a href=\"" . $tag_url . "\" class=\"pullRight\">";
            if ($tag != null) $html .= $tag . "</a>";
            $html .= "<div class=\"mainSocial pullLeft\">
                                        <a href=\"".get_permalink()."?share=true&sharer=twitter&id\"".get_the_ID()."\"><i class=\"icon-twitter\"></i></a>
												<a href=\"".get_permalink()."?share=true&sharer=facebook&id=\"".get_the_ID()."\"><i class=\"icon-facebook-official\"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                   </div>";
        $x++;

    }
    $response['html'] = $html;
    $response['no_more'] = false;
    if($page < $posts->max_num_pages)
        $response['no_more'] = true;
    wp_send_json($response);
}
function pload_more_ajax_handler()
{

    $page = $_POST['page']+1;
    $args = array(
        'post_type' => 'photo',
        'posts_per_page' => 12
    );
    $posts = new WP_Query($args);
    $html = "";
    $response = array();
    while($posts->have_posts()){
        $posts->the_post();
        $html .= " <div class=\"mycol-lg-4 mycol-sm-6\">
                <div class=\"photosPost mrgBtm\">
                    <div class=\"avatar\"><img src=\"".get_the_post_thumbnail_url()."\" class=\"bgCover fullWidth\" alt=\"\"></div>
                    <div class=\"content\">
                        <div class=\"galleryNums\">
                            <div><i class=\"icon-photoicon\"></i></div>
                        </div>
                        <div class=\"title\">
                            <a href=\"".get_permalink()."\" class=\"openPopup\">".get_the_title()."</a>
                        </div>
                    </div>
                </div>
            </div>";

    }
    $response['html'] = $html;
    $response['no_more'] = false;
    if($page < $posts->max_num_pages)
        $response['no_more'] = true;
    wp_send_json($response);
}
function contact_ajax_handler(){
    if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
    {

        $contact = array(
            'post_title' => $_POST['name'],
            'post_type' => 'contact',
            'post_status' => 'publish'
        );

        $post_id = wp_insert_post($contact);

        $field_key = "name";
        $value = $_POST['name'];
        update_field( $field_key, $value, $post_id );


        $field_key = "number";
        $value = $_POST['number'];
        update_field( $field_key, $value, $post_id );

        $field_key = "email";
        $value = $_POST['email'];
        update_field( $field_key, $value, $post_id );

        $field_key = "message";
        $value = $_POST['message'];
        update_field( $field_key, $value, $post_id );

        $field_key = "address";
        $value = $_POST['address'];
        update_field( $field_key, $value, $post_id );

        wp_send_json(['success' => true]);

    } else{
        wp_send_json(['success' => false]);
    }
}