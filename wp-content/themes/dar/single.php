<script>
    if(<?= $_GET['share']?> == true){
        console.log(true);

        if("<?= $_GET['sharer']?>" == "facebook")window.location.replace("https://www.facebook.com/sharer/sharer.php?u=<?= get_permalink($_GET['id'])?>");
        else if ("<?= $_GET['sharer']?>" == "twitter" )window.location.replace("http://twitter.com/home/?status=<?= get_permalink($_GET['id']); ?>");
    }
</script>
<?php
get_header();
setPostViews(get_the_ID());
$time = get_post_diff(get_post_time("U", true, get_the_ID()));
$dur = $time[0];
$time = $time[1];
if(has_tag())
    $tags = get_the_tags();
?>
<div class="mainContent">
    <div class="gridContainer">

        <div class="myrow clearfix">

            <div class="mycol-lg-6 mycol-xmd-8">
                <div class="theArticleDetails white-bg">
                    <div class="avatar"><img src="<?php echo get_the_post_thumbnail_url();?>" alt=""></div>
                    <div class="content">
                        <h1 class="title"><?php echo get_the_title();?> </h1>
                        <div class="date"><?php echo "منذ ".$time." ".$dur?> </div>
                        <div class="description single" >
                           <?php
                           echo get_post()->post_content;
                           ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mycol-lg-3 mycol-xmd-4">

                <div class="white-bg mainPadding articleData mrgBtm">

                    <div class="mainSocial">
                        <a href="http://twitter.com/home/?status=<?= get_the_title(); ?> - <?= get_permalink()?>"><i class="icon-twitter"></i></a>
                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?= get_permalink()?>"><i class="icon-facebook-official"></i></a>
                    </div>

                    <hr>

                    <div class="brand-color font-4 mrgBtm-md">كلمات مفتاحية</div>
                    <div class="font-medium keywords">
                        <?php if(has_tag()){foreach ($tags as $tag){
                        echo "<a href='".get_tag_link($tag->term_id)."'>$tag->name</a>";
                        }}?>
                    </div>

                    <hr>

                    <div class="brand-color font-4 mrgBtm-md">الرابط</div>
                    <div class="linkURL"><input type="text" value="<?= get_permalink()?>"></div>

                    <hr>

                    <div class="print clearfix">
                        <div class="pullRight brand-color font-4">الطباعة</div>
                        <a href="#" class="pullLeft" onclick="window.print()">طباعة هذه الصفحة</a>
                    </div>

                </div>

                <div class="worldsNews mrgBtm">
                    <div class="mainTitle"><h2 class="name">الأكثر قراءة</h2></div>
                    <?php
                    $query = array(
                            'meta_key' => 'post_views_count',
                            'orderby' => 'meta_value_num',
                            'order' => 'DESC',
                            'post_type' => 'news',
                            'posts_per_page' => 5
                         );
                    $posts = new WP_Query($query);
                    if ($posts->have_posts()){
                        $x = 0;
                      while ($posts->have_posts()){
                          $posts->the_post();
                          $time = get_post_diff(get_post_time("U", true, get_the_ID()));
                          $dur = $time[0];
                          $time = $time[1];
                          $cat = get_the_category()[0]->name;
                          $caturl = get_category_link(get_the_category()[0]->term_id);
                          ?>
                          <div class="mainPost">

                        <?php if($x == 0){?><div class="avatar"><img src="<?php echo get_the_post_thumbnail_url()?>" alt=""></div><?php }?>
                        <div class="content">
                            <div class="titleArea">
                                <div class="title"><a href="<?php get_permalink()?>"><?php echo get_the_title()?> </a></div>
                                <div class="date"><?php echo "منذ ".$time." ".$dur?> </div>
                            </div>
                            <div class="shareAndSection clearfix">
                                <a href="<?= $caturl?>" class="pullRight"><?php echo $cat; ?></a>
                                <div class="mainSocial pullLeft">
                                    <a href="http://twitter.com/home/?status=<?= get_the_title(); ?> - <?= get_permalink()?>"><i class="icon-twitter"></i></a>
                                    <a href="https://www.facebook.com/sharer/sharer.php?u=<?= get_permalink()?>"><i class="icon-facebook-official"></i></a>
                                </div>
                            </div>
                        </div>

                    </div>
                      <?php $x++;}} ?>
                </div>

            </div>

            <div class="mycol-lg-3 ipadProHide">

                <div class="oneWidget trend mrgBtm">
                    <div class="mainTitle">
                        <div class="name">الاكثر انتشارا</div>
                    </div>
                    <hr>
                    <div class="content mainPadding font-1">
                        <?php
                        $tags = get_terms(array(
                            'taxonomy' => 'post_tag',
                            'orderby' => 'count',
                            'order' => 'DESC',
                        ));
                        $x = 0;
                        foreach ( (array) $tags as $tag ) {
                            if($x < 4)
                                echo '<a href="' . get_tag_link ($tag->term_id) . '" rel="tag">#' . $tag->name . '</a>';
                            $x++;
                        }
                        ?>
                    </div>
                </div>


            </div>

        </div>

        <div class="mainTitle mrgBtm"><div class="name">أخبار متعلقة</div></div>

        <div class="myrow clearfix">
            <?php $cat = get_the_category();
                $query = array(
                        'post_type' => 'news',
                        'cat' => $cat[0]->term_id,
                        'posts_per_page' => 4);
                $posts = new WP_Query($query);
                    $x = 0;
                while($posts->have_posts()){
                    $posts->the_post();
                    $tag = null;
                    if(has_tag()) {
                        $tag = get_the_tags()[0]->name;
                        $tag_url = get_tag_link(get_the_tags()[0]->term_id);
                    }
                    $time = get_post_diff(get_post_time("U", true, get_the_ID()));
                    $dur = $time[0];
                    $time = $time[1];
                ?>
            <div class="mycol-lg-3 mycol-sm-6">
                <div class="mainPost mrgBtm">
                    <?php if($x % 2 == 0){?><div class="avatar"><img src="<?php echo get_the_post_thumbnail_url()?>" class="bgCover" alt=""></div><?php }?>
                    <div class="content">
                        <div class="titleArea">
                            <div class="title"><a href="<?php echo get_permalink()?>"><?php echo get_the_title()?> </a></div>
                            <div class="time"><?php echo "منذ ".$time." ".$dur?></div>
                        </div>
                        <?php if($x % 2 != 0){?>
                        <div class="description">
                            <div class="in">
                                <p><?php echo get_the_content()?></p>
                            </div>
                        </div>
                        <?php }?>
                        <div class="shareAndSection clearfix">
                            <a href="<?= $tag_url?>" class="pullRight"><?php if($tag != null) echo $tag?></a>
                            <div class="mainSocial pullLeft">
                                <a href="<?php echo get_permalink()."?share=true&sharer=twitter&id".get_the_ID()?>"><i class="icon-twitter"></i></a>
                                <a href="<?php echo get_permalink()."?share=true&sharer=facebook&id=".get_the_ID()?>"><i class="icon-facebook-official"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $x++;}?>
        </div>

    </div>
</div>
<script>
    if($('.single img').hasClass('aligncenter'))
    {
        $('.single img').css('margin','0 auto');
    }
</script>
<?php get_footer()?>
