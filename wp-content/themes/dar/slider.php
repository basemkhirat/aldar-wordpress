<?php
$args = array(
    'post_type' => 'main-slider',
    //'post_per_page' => 3
);
$sliders = new WP_Query($args);
if($sliders->have_posts()){
    $sliders->the_post();
    $tag = null;
    if(has_tag())
    {
        $tag = get_the_tags()[0]->name;
        $tag_url = get_tag_link(get_the_tags()[0]->term_id);
    }
    $time = get_post_diff(get_post_time("U", true, get_the_ID()));
    $dur = $time[0];
    $time = $time[1];
    ?>
                <div class="mrgBtm">
                    <div class="flexslider normalSlider mainSlider">
                        <ul class="slides">
                            <li>
                                <div class="mainPost sliderPost clearfix">

                                    <div class="avatar"><img src="<?php echo get_the_post_thumbnail_url()?>" class="bgCover" alt=""></div>
                                    <div class="content">
                                        <div class="titleArea">
                                            <h3 class="title"><a href="<?php echo get_permalink()?>"><?php echo get_the_title()?> </a></h3>
                                            <div class="time"><?php echo "منذ ".$time." ".$dur?> </div>
                                        </div>
                                        <div class="description">
                                            <div class="in">
                                                <p><?php echo get_the_content()?></p>
                                            </div>
                                        </div>
                                        <div class="shareAndSection clearfix">
                                            <a href="<?= $tag_url?>" class="pullRight"><?php if($tag != null) echo $tag?></a>
                                            <div class="mainSocial pullLeft">
                                                <a href="<?php echo get_permalink()."?share=true&sharer=twitter&id".get_the_ID()?>"><i class="icon-twitter"></i></a>
                                                <a href="<?php echo get_permalink()."?share=true&sharer=facebook&id=".get_the_ID()?>"><i class="icon-facebook-official"></i></a>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </li>
                            <?php while ($sliders->have_posts()){
                                $sliders->the_post();
                                $tag = null;
                                if(has_tag()) {
                                    $tag = get_the_tags()[0]->name;
                                    $tag_url = get_tag_link(get_tag(16)->term_id);
                                }
                                $time = get_post_diff(get_post_time("U", true, get_the_ID()));
                                $dur = $time[0];
                                $time = $time[1];
                                ?>
                            <li>
                                <div class="mainPost sliderPost clearfix">

                                    <div class="avatar"><img src="<?php echo get_the_post_thumbnail_url()?>" class="bgCover" alt=""></div>
                                    <div class="content">
                                        <div class="titleArea">
                                            <h3 class="title"><a href="<?php echo get_permalink()?>"><?php echo get_the_title() ?> </a></h3>
                                            <div class="time"><?php echo "منذ ".$time." ".$dur?> </div>
                                        </div>
                                        <div class="description">
                                            <div class="in">
                                                <p><?php echo get_the_content()?></p>
                                            </div>
                                        </div>
                                        <div class="shareAndSection clearfix">
                                            <a href="<?= $tag_url?>" class="pullRight"><?php if($tag != null) echo $tag?></a>
                                            <div class="mainSocial pullLeft">
                                                <a href="<?php echo get_permalink()."?share=true&sharer=twitter&id".get_the_ID()?>"><i class="icon-twitter"></i></a>
                                                <a href="<?php echo get_permalink()."?share=true&sharer=facebook&id=".get_the_ID()?>"><i class="icon-facebook-official"></i></a>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </li>
        <?php }?>
                        </ul>
                    </div>
                </div>

<?php }?>