<?php
$section = get_post(8)->post_title;
$section_name = get_post(8)->post_name;
$args = array('post_type' => $section_name);
$posts = new WP_Query($args);
$url = get_permalink(152);
if($posts->have_posts()){
?>
<div class="white-bg paddBtm mrgBtm">
    <div class="gridContainer">
        <div class="mainTitle mrgBtm hasBorder clearfix">
            <div class="name pullRight"><?php echo $section?></div>
            <a href="<?php echo $url?>" class="more pullLeft">مشاهده الكل</a>
        </div>
        <div class="videosSlider">
            <div id="slider" class="flexslider">
                <ul class="slides">
                    <?php while($posts->have_posts()){
                        $posts->the_post();

                    ?>
                    <li data-thumb="<?php echo get_the_post_thumbnail_url()?>">
                        <div class="videoSliderPost">
                            <div thumb="<?php echo get_the_post_thumbnail_url()?>"
                                 content="<?php echo get_the_content()?>"
                                 title="<?php echo get_the_title()?>"
                                 id="<?php echo get_the_ID()?>"
                                 class="avatar openPopup" style="cursor: pointer">
                                <img src="<?php echo get_the_post_thumbnail_url()?>" class="bgCover" alt=""></div>
                            <div class="content">
                                <a class="title clearfix">
                                    <i class="icon-right-dir"></i>
                                    <span>
                                        <?php echo get_the_title()?>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </li>

        <?php }?>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php }?>

