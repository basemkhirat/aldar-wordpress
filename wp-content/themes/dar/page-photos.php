<?php
get_header();
$args = array(
    'post_type' => 'photo',
    'posts_per_page'=> 12
);
$photos = new WP_Query($args);
$count = $photos->post_count;
$c = 1;
if($photos->have_posts()){
?>
    <div class="thePopup" style="display: block;min-height: 100%;overflow: scroll;opacity: 0;z-index: -1;">
        <div class="noBg">
         <div class="popupIframe" >
        <div class="verticalCentered">
            <div class="theCell">
                <div class="gridContainer">

                    <div><div class="closePopupp"style="width: 45px;line-height: 45px;text-align: center;display: inline-block;background: #C73133;color: #fff;"><i class="icon-close"></i></div></div>

                    <div class="flexslider photoGallerySlider mrgBtm">
                        <ul class="slides" >
                            <?php while($photos->have_posts()){
                            $photos->the_post();
                            ?>
                            <li index="<?= $c?>">
                                <div class="photosPost" >
                                    <div class="avatar"><img src="<?php echo get_the_post_thumbnail_url();?>" alt="" ></div>
                                    <div class="content">
                                        <div class="title">
                                            <a href="#"><?php echo get_the_title()?></a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <?php $c++; if($c > 6) break; }?>
                        </ul>
                    </div>

                    <div id="carousel" class="flexslider photoGalleryThum">
                        <ul class="slides">
                            <?php $c = 1; $photos = new WP_Query($args);
                            while($photos->have_posts()){
                            $photos->the_post();
                            ?>
                            <li>
                                <img src="<?php echo get_the_post_thumbnail_url();?> "  />
                            </li>
                            <?php $c++; if($c > 6) break; }?>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
        </div>
    </div>

<div class="mainContent">

    <div class="white-bg mrgBtm">
        <div class="gridContainer">

            <div class="albumsSlider downSpaceSlider">
                <div class="flexslider carousel carouselThree downPosition">
                    <ul class="slides" >
                        <?php $c = 1;$photos = new WP_Query($args);
                        while($photos->have_posts()){
                            $photos->the_post();
                            ?>
                        <li id="opp" index="<?= $c?>">
                            <div class="photosPost">
                                <a style="cursor: pointer" class="avatar openPopupp"><img src="<?php echo get_the_post_thumbnail_url();?>" class="bgCover" alt=""></a>
                                <div class="content">
                                    <div class="galleryNums">
                                        <div><i class="icon-photoicon"></i></div>
                                        <div><?php echo $c."/".$count?></div>
                                    </div>
                                    <div class="title">
                                        <a href="<?php echo get_permalink()?>" class=""><?php echo get_the_title()?></a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <?php $c++; if($c > 6) break; }?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <script>
        					//position index for desired slide goes here

        $(document).on('click','#opp',function (e) {
            console.log("SS");
            $('.thePopup').css('opacity','1');
            $('.thePopup').css('z-index','999999999');
            var slider = $('.flexslider').data('flexslider');
            console.log($(this).attr("index"));
            slider.flexAnimate(parseInt($(this).attr("index"))-1);
        });
        $(document).on('click', '.closePopupp', function (e) {
            $('.thePopup').css('opacity','0');
            $('.thePopup').css('z-index','-1');
        });
        $('.flexslider .slides').addClass('flexslider-fix-rtl');
        $(document).ready(function() {
            $('.photoGalleryThum').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshow: false,
                directionNav: false,
                itemWidth: 135,
                itemMargin: 20,
                asNavFor: '.photoGallerySlider',
                rtl: true,
                start: function(slider){
                    $('.slides', slider).removeClass('flexslider-fix-rtl');
                }
            });

            $('.photoGallerySlider').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshow: false,
                sync: ".photoGalleryThum",
                rtl: true,
                start: function(slider){
                    $('.slides', slider).removeClass('flexslider-fix-rtl');
                    $('.content', slider).prepend('<div class="galleryNums"><div><i class="icon-photoicon"></i></div><div><span class="currentSlide">' + (slider.currentSlide+1) + '</span>/<span>'+ slider.count +'</span></div></div>');
                },
                after: function(slider){
                    $('.currentSlide', slider).html(slider.currentSlide+1);
                }
            });
        });
    </script>
    <div class="gridContainer">
        <div class="myrow clearfix">
            <?php while($photos->have_posts()){
                $photos->the_post();
            ?>
            <div class="mycol-lg-4 mycol-sm-6">
                <div class="photosPost mrgBtm">
                    <div class="avatar"><img src="<?php echo get_the_post_thumbnail_url();?>" class="bgCover" alt=""></div>
                    <div class="content">
                        <div class="galleryNums">
                            <div><i class="icon-photoicon"></i></div>
                            <div><?php echo $c."/".$count?></div>
                        </div>
                        <div class="title">
                            <a href="<?php echo get_permalink()?>" class=""><?php echo get_the_title()?></a>
                        </div>
                    </div>
                </div>
            </div>
            <?php $c++;}?>
            <div class="more" style="display: none">
                <input name="post_type" id="post_type" value="photo">
            </div>
        </div>

        <div id="pload_more" style="cursor: pointer"> <a class="mainBtn">المزيد</a></div>

    </div>
</div>
<?php }get_footer()?>
