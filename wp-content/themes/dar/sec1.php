<?php
$args = array(
    'post_type' => 'news',
    'cat' => 2,
    'posts_per_page' => 6
);
$cat = get_cat_name(2);
$url = get_category_link(2);
$news = new WP_Query($args);
if($news->have_posts()){
    $news->the_post();
    $tag = null;
    if(has_tag()) {
        $tag = get_the_tags()[0]->name;
        $tag_url = get_tag_link(get_the_tags()[0]->term_id);
    }
    $time = get_post_diff(get_post_time("U", true, get_the_ID()));
    $dur = $time[0];
    $time = $time[1];
    $x = 0;
    ?>
    <div>

						<div class="mainTitle mrgBtm clearfix">
							<div class="name pullRight"><?php echo $cat?></div>
							<a href="<?= $url?>" class="more pullLeft">مشاهده الكل</a>
						</div>

						<div class="myrow clearfix">

							<div class="mycol-xmd-8">
								<div class="mainPost bigPost mrgBtm">

									<div class="avatar"><img src="<?php echo get_the_post_thumbnail_url()?>" class="bgCover" alt=""></div>
									<div class="content">
										<div class="titleArea">
											<div class="title"><a href="<?php echo get_permalink()?>"><?php echo get_the_title()?> </a></div>
											<div class="time"><?php echo "منذ ".$time." ".$dur?> </div>
										</div>
										<div class="description">
											<div class="in">
												<p><?php echo get_the_excerpt()?></p>
											</div>
										</div>
										<div class="shareAndSection clearfix">
											<a href="<?= $tag_url?>" class="pullRight"><?php if($tag != null) echo $tag?></a>
											<div class="mainSocial pullLeft">
												<a href="<?php echo get_permalink()."?share=true&sharer=twitter&id".get_the_ID()?>"><i class="icon-twitter"></i></a>
												<a href="<?php echo get_permalink()."?share=true&sharer=facebook&id=".get_the_ID()?>"><i class="icon-facebook-official"></i></a>
											</div>
										</div>
									</div>

								</div>
							</div>
                            <?php while($news->have_posts()){
                                $news->the_post();
                                $tag = null;
                                if(has_tag()) {
                                    $tag = get_the_tags()[0]->name;
                                    $tag_url = get_tag_link(get_the_tags()[0]->term_id);
                                }
                                $time = get_post_diff(get_post_time("U", true, get_the_ID()));
                                $dur = $time[0];
                                $time = $time[1];
                                if($x % 2 == 0 ){
                            ?>
							<div class="mycol-xmd-4 mycol-sm-6">
								<div class="mainPost mrgBtm">

									<div class="avatar"><img src="<?php echo get_the_post_thumbnail_url()?>" class="bgCover" alt=""></div>
									<div class="content">
										<div class="titleArea">
											<div class="title"><a href="<?php echo get_permalink()?>"><?php echo get_the_title()?> </a>
                                            </div>
											<div class="time"><?php echo "منذ ".$time." ".$dur?> </div>
										</div>
										<div class="shareAndSection clearfix">
											<a href="<?= $tag_url?>" class="pullRight"><?php if($tag != null) echo $tag?></a>
											<div class="mainSocial pullLeft">
                                                <a href="<?php echo get_permalink()."?share=true&sharer=twitter&id".get_the_ID()?>"><i class="icon-twitter"></i></a>
                                                <a href="<?php echo get_permalink()."?share=true&sharer=facebook&id=".get_the_ID()?>"><i class="icon-facebook-official"></i></a>
											</div>
										</div>
									</div>

								</div>
							</div>
                                    <?php }else {?>
							<div class="mycol-xmd-4 mycol-sm-6">
								<div class="mainPost mrgBtm">

									<div class="content">
										<div class="titleArea">
											<div class="title"><a href="<?php echo get_permalink()?>"><?php echo get_the_title()?> </a></div>
											<div class="time"><?php echo "منذ ".$time." ".$dur?> </div>
										</div>
										<div class="description">
											<div class="in">
												<p><?php echo get_the_excerpt()?></p>
											</div>
										</div>
										<div class="shareAndSection clearfix">
											<a href="#" class="pullRight"><?php if($tag != null) echo $tag?></a>
											<div class="mainSocial pullLeft">
                                                <a href="<?php echo get_permalink()."?share=true&sharer=twitter&id".get_the_ID()?>"><i class="icon-twitter"></i></a>
                                                <a href="<?php echo get_permalink()."?share=true&sharer=facebook&id=".get_the_ID()?>"><i class="icon-facebook-official"></i></a>
											</div>
										</div>
									</div>

								</div>
							</div>
                            <?php }$x++;}?>
						</div>
    </div>
<?php }?>

