<?php
get_header();
$arabic = explode('.',get_post_meta(470,'arabic',true));
$eng = explode('.',get_post_meta(470,'english',true));


?>
<div class="mainContent">
    <div class="gridContainer">
        <div class="halfDisplay">

            <div class="oneHalf">
                <div class="aboutBox">
                    <?php foreach ($arabic as $ar)
                            echo "<p>".$ar.".</p>";
                            ?>
                </div>
            </div>

            <div class="oneHalf">
                <div class="aboutBox ltrDir">
                    <?php foreach ($eng as $en)
                        echo "<p>".$en.".</p>";
                    ?>
                </div>
            </div>

        </div>
    </div>
</div>
<?php get_footer();?>
