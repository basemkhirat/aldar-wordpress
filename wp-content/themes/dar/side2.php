<?php
$tags = get_terms(array(
    'taxonomy' => 'post_tag',
    'orderby' => 'count',
    'order' => 'DESC',
));
$x = 0;
?>
<div class="mycol-lg-3 ipadProHide">

    <div class="oneWidget trend mrgBtm">
        <div class="mainTitle">
            <div class="name">الاكثر انتشارا</div>
        </div>
        <hr>
        <div class="content mainPadding font-1">
           <?php foreach ( (array) $tags as $tag ) {
             if($x < 4)
                 echo '<a href="' . get_tag_link ($tag->term_id) . '" rel="tag">#' . $tag->name . '</a>';
             $x++;
            }
            ?>

        </div>
    </div>
</div>
</div>
</div>
</div>