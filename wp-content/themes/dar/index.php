<?php
get_header();
?>
<div class="mainContent">
    <div class="gridContainer">
        <div class="myrow clearfix">

            <div class="mycol-lg-9">
<?php
require 'slider.php';
require 'sec1.php';
?>
            </div>
            <div class="mycol-lg-3 ipadProHide">
                <?php require 'side1.php';?>
            </div>
        </div>
    </div>
    <?php require 'videos.php';?>
    <?php require 'sec2.php';?>
    <?php // require 'side2.php';?>
    <?php require 'blogs.php';?>
    <?php require 'infographic.php';?>
    <?php require 'sec3.php';?>
</div>
    <?php
get_footer();
