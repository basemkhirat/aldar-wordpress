<?php
get_header();
$contact = get_post_meta(472,'للتواصل والمعلومات', true);
$adv = get_post_meta(472,'للإعلانات', true);
$affairs = get_post_meta(472,'للشئون التحريرية', true);
?>
<div class="mainContent">
    <div class="gridContainer">

        <div class="white-bg mainPadding contactPage">
            <div class="myrow clearfix">

                <div class="mycol-lg-6" id="form1">
                    <form action="#" class="mrgBtm">
                        <div class="myrow clearfix">


                            <div class="mycol-md-6">
                                <input id="name" type="text" placeholder="الاسم" class="formItem mrgBtm">
                            </div>

                            <div class="mycol-md-6">
                                <input id="email" type="text" placeholder="البريد الإلكتروني" class="formItem mrgBtm">
                            </div>

                            <div class="mycol-md-6">
                                <input id="number" type="text" placeholder="الهاتف الجوال" class="formItem mrgBtm">
                            </div>

                            <div class="mycol-md-6">
                                <input id="address" type="text" placeholder="المكان" class="formItem mrgBtm">
                            </div>

                            <div class="mycol-lg-12">
                                <textarea id="message" class="formItem textarea mrgBtm" placeholder="الرسالة"></textarea>
                            </div>
                            <div class="alert alert-danger " id="error" style="display: none">
                                * برجاء مراجعة و ادخال جميع البيانات
                            </div>
                            <div class="mycol-lg-12 contact">
                                <input type="submit" class="formItem submit" value="إرسال">
                            </div>

                        </div>
                    </form>
                </div>

                <div class="mycol-lg-6">
                    <div class="myrow clearfix">

                        <div class="mycol-sm-6">
                            <div class="brand-color font-1 mrgBtm">للتواصل والمعلومات</div>
                            <div class="mrgBtm font-medium"><a href="#"><?= $contact?></a></div>
                            <div class="brand-color font-1 mrgBtm">للإعلانات</div>
                            <div class="mrgBtm font-medium"><a href="#"><?= $adv?></a></div>
                            <div class="brand-color font-1 mrgBtm">للشئون التحريرية</div>
                            <div class="mrgBtm font-medium"><a href="#"><?= $affairs?></a></div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

    </div>
</div>
<?php get_footer()?>

