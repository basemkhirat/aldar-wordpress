$ = jQuery;
$(document).on('click','#load_more',function(){
    post_type =  document.getElementById('post_type').value;
    name1 =  document.getElementById('name').value;
    ppp =  document.getElementById('ppp').value;
    $('#load').remove();
    var button = $(this),
        data = {
            'action': 'load_more',
            'page' : load_more.current_page,
            'post_type':post_type,
            'name': name1,
            'ppp': ppp
        };
    console.log(load_more.ajaxurl);
    $.ajax({
        url : load_more.ajaxurl, // AJAX handler
        data : data,
        type : 'POST',
        success : function( data ){
            if( data  ) {
                load_more.current_page++;
                $(data.html).insertBefore(".add").hide().fadeIn(2000);
                if(data.no_more === false) $('#load_more').remove();
            }else{
            }
        }
    });
});
