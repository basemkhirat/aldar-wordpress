
$(document).on('click', ".contact", function (e) {
    e.preventDefault();
    document.getElementById('error').style.display = 'none';

    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    var numberReg = /^\d*(?:\.\d{1,2})?$/;

    var x = true;

    formData = {
        'action': 'contact',
        'name': $('#name').val(),
        'email': $('#email').val(),
        'message': $('#message').val(),
        'number': $('#number').val(),
        'address': $('#address').val(),
    };

    if(!emailReg.test(formData.email) || formData.email == '' ||
        !numberReg.test(formData.number) || formData.full_name == '' ||
        formData.message == '' || formData.address == '')
        x = false;
    console.log(x);
    if(!x){
        document.getElementById('error').style.display = 'block';
        return;
    }



    $('.cloader').fadeIn(800);

    $.ajax({
        url: contact_params.ajaxurl, // AJAX handler
        data: formData,
        type: 'POST',
        success: function (data) {
            if (data.success == true) {
                $('#form1').empty();
                $('#form1').append('<div class="brand-color font-1 mrgBtm" style="font-size: 80px; text-align: center">شكرا لك</div>');
            }
        },
        error: function(data){
        }
    });
});


