
function toggleClassToBody(className) {
        var theBody = document.getElementsByTagName('body').item(0);
        theBody.classList.toggle(className);
    }
    $ = jQuery;
        $(window).load(function() {
            $('.loading').fadeOut(2000);
        });
    $(document).on('click','.openPopup',function (e) {
        e.preventDefault();
        var thumb = $(this).attr('thumb');
        var title = $(this).attr('title');
        var content = $(this).attr('content');
        $('body').append(' <div  class="thePopup"> ' +
            '<span class="closePopup">x</span> ' +
            '<div class="popupIframe"> ' +
            '<div class="verticalCentered"> ' +
            '<div class="theCell"> ' +
            '<div class="gridContainer"> ' +
            '<div> ' +
            '<div class="closePopup"> ' +
            '<i class="icon-close"> ' +
            '</i> </div> </div> ' +
            '<div class="videoPopup"> ' +
            '<div class="avatar active"> ' +
            '<img src="'+thumb+'" > ' +
            '<span class="icon"> <i class="icon-right-dir"> </i> </span> ' +
            '<iframe width="560" height="315" src="'+content+'" frameborder="0" allowfullscreen> </iframe> </div> ' +
            '<div class="title">'+title+'</div> </div> <!----> <!----> </div> </div> </div> </div> </div>');
    });

    $(document).on('click', '.closePopup', function (e) {
        $('.thePopup').remove();
    });

jQuery(document).ready(function($) {
    // Code that uses jQuery's $ can follow here.

    var rtl = !$('body').hasClass('english');
    $('.flexslider .slides').addClass('flexslider-fix-rtl');


    $(window).load(function () {
        $('.hourlyWeater .flexslider').flexslider({
            animation: "slide",
            controlNav: false,
            directionNav: true,
            slideshow: false,
            rtl: rtl,
            start: function (slider) {
                $('.slides', slider).removeClass('flexslider-fix-rtl');
                slider.resize();
            }
        });

        $('.normalSlider.flexslider').flexslider({
            animation: "slide",
            controlNav: false,
            directionNav: true,
            slideshow: false,
            rtl: rtl,
            start: function (slider) {
                $('.slides', slider).removeClass('flexslider-fix-rtl');
            }
        });

        $('.carouselFour.flexslider').flexslider({
            animation: "slide",
            controlNav: false,
            itemWidth: 290,
            itemMargin: 20,
            maxItems: 4,
            minItems: 1,
            rtl: rtl,
            start: function (slider) {
                $('.slides', slider).removeClass('flexslider-fix-rtl');
            }
        });

        $('.carouselThree.flexslider').flexslider({
            animation: "slide",
            controlNav: false,
            itemWidth: 393,
            itemMargin: 20,
            maxItems: 3,
            minItems: 1,
            rtl: rtl,
            start: function (slider) {
                $('.slides', slider).removeClass('flexslider-fix-rtl');
            }
        });

        $('#slider').flexslider({
            animation: "slide",
            controlNav: "thumbnails",
            direction: 'vertical',
            rtl: rtl,
            start: function (slider) {
                $('.slides', slider).removeClass('flexslider-fix-rtl');
            }
        });

    });


    /*
    $('body').on('click', '.closePopup', function(){
        $('.thePopup').remove();
    });

    function readURL(ele, input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.' + ele).attr('src', e.target.result).prev('.hideAfterUpload').addClass('disNone');
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $('.importItemsForm input[type="file"]').change(function(){
        var theFileUrl = $(this).val();
        $(this).next('.uploadedFileDisplay').html(theFileUrl);
    });
    */

    /*--------------------------------------------------------
        Image plugin
    --------------------------------------------------------*/
    var imgObj, divEle, divEle_width, divEle_height, imgEle, imgEle_width, imgEle_height,
        eles = document.getElementsByClassName('bgCover');
    for (i = 0; i < eles.length; i++) {
        eles.item(i).parentNode.style.background = "#ddd";
    }

    function getImages() {
        for (i = 0; i < eles.length; i++) {

            imgEle = eles[i];
            divEle = imgEle.parentNode;

            divEle_width = divEle.offsetWidth;
            divEle_height = divEle.offsetHeight;

            imgObj = new Image();
            imgObj.src = imgEle.getAttribute('src');
            imgEle_width = imgObj.width;
            imgEle_height = imgObj.height;

            if (divEle_height / imgEle_height > divEle_width / imgEle_width) {
                imgEle.classList.remove("fullWidth");
                imgEle.classList.remove("fullHeight");
                imgEle.className += " fullHeight";
                imgEle.style.marginLeft = ((imgEle.clientWidth - divEle_width) / 2 * -1) + "px";
                imgEle.style.marginTop = 0;
            }
            else {
                imgEle.classList.remove("fullWidth");
                imgEle.classList.remove("fullHeight");
                imgEle.className += " fullWidth";
                imgEle.style.marginTop = ((imgEle.clientHeight - divEle_height) / 2 * -1) + "px";
                imgEle.style.marginLeft = 0;
            }
        }
    }

    window.addEventListener("load", getImages);
    window.addEventListener("resize", getImages2);

    var loadCounter = 0;
    $(window).load(function (e) {
        var imagePlg = setInterval(function (e) {
            if (loadCounter < 5) {
                getImages();
            }
            else {
                clearInterval(imagePlg);
            }
            loadCounter++;
        }, 500);
    })

    function getImages2() {
        var loadCounter2 = 0;
        clearInterval(imagePlg2);
        var imagePlg2 = setInterval(function (e) {
            if (loadCounter2 < 2) {
                getImages();
            }
            else {
                clearInterval(imagePlg2);
            }
            loadCounter2++;
        }, 1000);
    }
});