$ = jQuery;
$(document).on('click','#cload_more',function(){
    post_type =  document.getElementById('post_type').value;
    name1 =  document.getElementById('name').value;
    ppp =  document.getElementById('ppp').value;
    cat_id = document.getElementById('cat_id').value

    $('#load').remove();
    var button = $(this),
        data = {
            'action': 'cat_load_more',
            'page' : cat_load_more.current_page,
            'post_type':post_type,
            'ppp': ppp,
            'cat_id' : cat_id,
            'name' : name1
        };

    $.ajax({
        url : cat_load_more.ajaxurl, // AJAX handler
        data : data,
        type : 'POST',
        success : function( data ){
            if( data  ) {
                console.log(data.html);
                cat_load_more.current_page++;
                $(data.html).insertBefore(".more").hide().fadeIn(2000);
                if(data.no_more === false) $('#cload_more').remove();
            }else{
            }
        }
    });
});
