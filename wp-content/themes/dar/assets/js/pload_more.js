$ = jQuery;
$(document).on('click','#pload_more',function(){
    post_type =  document.getElementById('post_type').value;
    $('#load').remove();
    var button = $(this),
        data = {
            'action': 'pload_more',
            'page' : pload_more.current_page,
            'post_type':post_type,
        };

    $.ajax({
        url : pload_more.ajaxurl, // AJAX handler
        data : data,
        type : 'POST',
        success : function( data ){
            if( data  ) {
                console.log(data.html);
                pload_more.current_page++;
                $(data.html).insertBefore(".more").hide().fadeIn(2000);
                if(data.no_more === false) $('#pload_more').remove();
            }else{
            }
        }
    });
});
