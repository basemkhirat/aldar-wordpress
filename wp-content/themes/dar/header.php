<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <title><?php bloginfo()?></title>
    <?php wp_head()?>
</head>
<div class="loading">
    <div class="verticalCentered">
        <div class="theCell textCentered">
            <div class="theImg">
               <a href="<?= site_url()?>"><img src="<?php echo get_template_directory_uri().'/assets/images/logoImg.png'?>" alt=""></a>
                <svg height="216" width="216">
                    <circle cx="108" cy="108" r="107" stroke="#C73133" stroke-width="2" stroke-dasharray="672" fill="none" />
                </svg>
            </div>
        </div>
    </div>
</div>
<body class="">
<header id="header">
    <div class="top">
        <div class="gridContainer clearfix">
           <a href="<?= site_url()?>"> <h1 class="logo pullRight"><img src="<?php echo get_template_directory_uri().'/assets/images/logo.png'?>" alt=""></h1></a>
            <div class="leftHead pullLeft">
                <div class="search clearfix">
                    <div class="icon pullLeft"><i class="icon-search"></i></div>
                    <?php require 'searchform.php' ?>
                </div>
                <?php
                $facebook = get_post_meta(589,'facebook',true);
                $linkedin = get_post_meta(589,'linkedin',true);
                $instagram = get_post_meta(589,'instagram',true);
                $twitter = get_post_meta(589,'twitter',true);
                $google = get_post_meta(589,'google plus',true);
                ?>
                <div class="mobileHide clearfix">
                    <div class="mainSocial itemBorder pullLeft">
                        <a href="<?php echo $linkedin?>" target="_blank"><i class="icon-linkedin"></i></a>
                        <a href="<?php echo $google?>" target="_blank"><i class="icon-gplus"></i></a>
                        <a href="<?php echo $instagram?>" target="_blank"><i class="icon-instagram"></i></a>
                        <a href="<?php echo $twitter?>" target="_blank"><i class="icon-twitter"></i></a>
                        <a href="<?php echo $facebook?>" target="_blank"><i class="icon-facebook-official"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    $items = wp_get_nav_menu_items('header');
    ?>
    <div class="brand-bg">
        <div class="gridContainer">
            <nav id="navigation">
                <div class="menuIcon" onClick="toggleClassToBody('menuOpened')">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <ul>
                    <?php foreach ($items as $item){
                        $out = "";
                        if(!menu_has_children($item->ID) && !is_menu_child($item->ID))
                            echo "<li><a href=\"".$item->url."\">".$item->title."</a></li>";
                        if(menu_has_children($item->ID)){
                            $children = get_menu_children($item->ID);
                            $out = "<li>
							<div class=\"mydropDown mainDropDown\">
								<a href=\"#\">".$item->title." <i class=\"icon-angledown\"></i></a>
								<div class=\"mdd_content\">";
                            foreach ($children as $child)
									$out .= "<a href=\"".$child['link']."\">".$child['title']."</a>";
								$out.="</div>
							</div>
						</li>";
								echo $out;
                        }
                    }?>
                </ul>
            </nav>
        </div>
    </div>
</header>
<?php
$cat_id = get_query_var('cat');
$tag_id = 0;
$tag_id = get_queried_object()->term_id;

if(!is_home() && !is_404()){?>
<div class="pageHir">
    <div class="gridContainer">
        <a href="<?php echo site_url()?>">الرئيسيه</a>
    <?php if((get_post_type() == "page")) {
        $name = get_post_meta(get_the_ID(), 'name', true);
        echo "<a href=''>$name</a>";
    }else if(is_tag() || is_category()) {
        if(is_tag()){
            echo "<a href='".get_tag_link($tag_id)."'>".get_tag($tag_id)->name."</a>";
        }else {
            $catName = get_cat_name($cat_id);
            $catLink = get_category_link($cat_id);
            echo "<a href='$catLink'>$catName</a>";
        }
        }else{
            $name = get_the_category()[0]->name;
            $link = get_category_link(get_the_category()[0]->term_id);
        echo "<a href='$link'>$name</a>";
    }
    ?>
    </div>
</div>
<?php }?>

    <div class="mobileNav white-color-all" id="mobileNav">
        <div class="gridContainer">

            <div class="theMenu">
                <ul>
                    <?php foreach ($items as $item){
                        $out = "";
                        if(!menu_has_children($item->ID) && !is_menu_child($item->ID))
                            echo "<li><a href=\"".$item->url."\">".$item->title."</a></li>";
                        if(menu_has_children($item->ID)){
                            $children = get_menu_children($item->ID);
                            $out = "<li>
							<div class=\"TM_section\">
								 <div class=\"name\">".$item->title." <i class=\"icon-angledown\"></i></div>
								<div class=\"subsection\">";
                            foreach ($children as $child)
                                $out .= "<a href=\"".$child['link']."\">".$child['title']."</a>";
                            $out.="</div>
							</div>
						</li>";
                            echo $out;
                        }
                    }?>
                </ul>
            </div>

            <?php
            $facebook = get_post_meta(589,'facebook',true);
            $linkedin = get_post_meta(589,'linkedin',true);
            $instagram = get_post_meta(589,'instagram',true);
            $twitter = get_post_meta(589,'twitter',true);
            $google = get_post_meta(589,'google plus',true);
            ?>
            <div class="mainSocial">
                <a href="<?php echo $linkedin?>" target="_blank"><i class="icon-linkedin"></i></a>
                <a href="<?php echo $google?>" target="_blank"><i class="icon-gplus"></i></a>
                <a href="<?php echo $instagram?>" target="_blank"><i class="icon-instagram"></i></a>
                <a href="<?php echo $twitter?>" target="_blank"><i class="icon-twitter"></i></a>
                <a href="<?php echo $facebook?>" target="_blank"><i class="icon-facebook-official"></i></a>
            </div>

        </div>
    </div>
