<?php
$arr = array();
$sund = get_post_meta(623,'Sunday',true);
$mon = get_post_meta(623,'Monday',true);
$tues = get_post_meta(623,'Tuesday',true);
$wedn = get_post_meta(623,'Wednesday',true);
$thurs = get_post_meta(623,'Thursday',true);
$fri = get_post_meta(623,'Friday',true);
$satur = get_post_meta(623,'Saturday',true);
$arr[] = $sund; $arr[] = $mon; $arr[] = $tues; $arr[] = $wedn; $arr[] = $thurs; $arr[] = $fri; $arr[] = $satur;
$details = get_post_meta(623,'details',true);
?>
<div class="oneWidget mrgBtm weather">
    <div class="mainTitle">
        <div class="name">الطقس</div>
    </div>
    <hr>
    <div class="mainPadding">
        <div class="tableDis">
            <div class="oneCell"><img src="images/widgets/weather.png" alt=""></div>
            <div class="oneCell brand-color textCentered dataTop">
                <div>المغرب</div>
                <div class="number"><div class="inlineBlock relative">26 <span>o</span></div></div>
                <div class="font-small"><?= $details?></div>
            </div>
            <div class="oneCell brand-color textCentered dataTop">
                <div>الخميس</div>
                <div class="number">15</div>
                <div class="font-small">مارس ٢٠١٦</div>
            </div>
        </div>
    </div>
    <div class="brand-bg hourlyWeater">
        <div class="flexslider arrows1 downPosition">
            <ul class="slides">
                <li>
                    <div class="one">
                        <div class="avatar"><img src="images/widgets/weather2.png" alt=""></div>
                        <div class="data">
                            <div class="time">9 AM</div>
                            <div class="number"><div class="inlineBlock relative">26 <span>o</span></div></div>
                        </div>
                    </div>

                </li>
                <li>
                    <div class="one">
                        <div class="avatar"><img src="images/widgets/weather2.png" alt=""></div>
                        <div class="data">
                            <div class="time">10 AM</div>
                            <div class="number"><div class="inlineBlock relative">26 <span>o</span></div></div>
                        </div>
                    </div>

                </li>
                <li>
                    <div class="one">
                        <div class="avatar"><img src="images/widgets/weather2.png" alt=""></div>
                        <div class="data">
                            <div class="time">11 AM</div>
                            <div class="number"><div class="inlineBlock relative">26 <span>o</span></div></div>
                        </div>
                    </div>

                </li>
                <li>
                    <div class="one">
                        <div class="avatar"><img src="images/widgets/weather2.png" alt=""></div>
                        <div class="data">
                            <div class="time">12 PM</div>
                            <div class="number"><div class="inlineBlock relative">26 <span>o</span></div></div>
                        </div>
                    </div>

                </li>
                <li>
                    <div class="one">
                        <div class="avatar"><img src="images/widgets/weather2.png" alt=""></div>
                        <div class="data">
                            <div class="time">1 PM</div>
                            <div class="number"><div class="inlineBlock relative">26 <span>o</span></div></div>
                        </div>
                    </div>

                </li>
            </ul>
        </div>
    </div>
    <div class="content">

        <div class="one mainPadding">
            <div class="tableDis">
                <div class="oneCell">الاحد</div>
                <div class="oneCell"><img src="images/widgets/weather.png" alt=""></div>
                <div class="oneCell number"><div class="inlineBlock relative"><?= explode('-',$arr[0])[0] ?><span>o</span></div></div>
                <div class="oneCell number"><div class="inlineBlock relative"><?= explode('-',$arr[0])[1] ?> <span>o</span></div></div>
            </div>
        </div>
        <div class="one mainPadding">
            <div class="tableDis">
                <div class="oneCell">التنين</div>
                <div class="oneCell"><img src="images/widgets/weather.png" alt=""></div>
                <div class="oneCell number"><div class="inlineBlock relative"><?= explode('-',$arr[1])[0] ?><span>o</span></div></div>
                <div class="oneCell number"><div class="inlineBlock relative"><?= explode('-',$arr[1])[1] ?> <span>o</span></div></div>
            </div>
        </div>
        <div class="one mainPadding">
            <div class="tableDis">
                <div class="oneCell">التلاتا</div>
                <div class="oneCell"><img src="images/widgets/weather.png" alt=""></div>
                <div class="oneCell number"><div class="inlineBlock relative"><?= explode('-',$arr[2])[0] ?><span>o</span></div></div>
                <div class="oneCell number"><div class="inlineBlock relative"><?= explode('-',$arr[2])[1] ?> <span>o</span></div></div>
            </div>
        </div>
        <div class="one mainPadding">
            <div class="tableDis">
                <div class="oneCell">الاربعا</div>
                <div class="oneCell"><img src="images/widgets/weather.png" alt=""></div>
                <div class="oneCell number"><div class="inlineBlock relative"><?= explode('-',$arr[3])[0] ?><span>o</span></div></div>
                <div class="oneCell number"><div class="inlineBlock relative"><?= explode('-',$arr[3])[1] ?> <span>o</span></div></div>
            </div>
        </div>
        <div class="one mainPadding">
            <div class="tableDis">
                <div class="oneCell">الخميس</div>
                <div class="oneCell"><img src="images/widgets/weather.png" alt=""></div>
                <div class="oneCell number"><div class="inlineBlock relative"><?= explode('-',$arr[4])[0] ?><span>o</span></div></div>
                <div class="oneCell number"><div class="inlineBlock relative"><?= explode('-',$arr[4])[1] ?> <span>o</span></div></div>
            </div>
        </div>
        <div class="one mainPadding">
            <div class="tableDis">
                <div class="oneCell">الجمعة</div>
                <div class="oneCell"><img src="images/widgets/weather.png" alt=""></div>
                <div class="oneCell number"><div class="inlineBlock relative"><?= explode('-',$arr[5])[0] ?><span>o</span></div></div>
                <div class="oneCell number"><div class="inlineBlock relative"><?= explode('-',$arr[5])[1] ?> <span>o</span></div></div>
            </div>
        </div>
        <div class="one mainPadding">
            <div class="tableDis">
                <div class="oneCell">السبت</div>
                <div class="oneCell"><img src="images/widgets/weather.png" alt=""></div>
                <div class="oneCell number"><div class="inlineBlock relative"><?= explode('-',$arr[6])[0] ?><span>o</span></div></div>
                <div class="oneCell number"><div class="inlineBlock relative"><?= explode('-',$arr[6])[1] ?> <span>o</span></div></div>
            </div>
        </div>
    </div>
</div>