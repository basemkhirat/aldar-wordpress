<?php
get_header();

$search = new WP_Query( array(
        's'=> $_GET['q'],
        'post_type' => 'news'
));
if($search->have_posts()){
    $x = 0;
?>
<div class="mainContent">
    <div class="gridContainer">

        <div class="mainPadding font-1 brand-color fontBold textCentered">نتائج البحث</div>

        <div class="myrow clearfix">
    <?php while($search->have_posts()){
        $search->the_post();
        $time = get_post_diff(get_post_time("U", true, get_the_ID()));
        $dur = $time[0];
        $time = $time[1];
        $tag = null;
        if(has_tag())
        {
            $tag = get_the_tags()[0]->name;
            $tag_url = get_tag_link(get_the_tags()[0]->term_id);
        }
        ?>
        <div class="mycol-xmd-4 mycol-sm-6">
            <div class="mainPost mrgBtm">

                <?php if($x % 2 != 0){
                    ?><div class="avatar"><img src="<?php echo get_the_post_thumbnail_url()?>" alt=""></div>
                <?php }?>
                <div class="content">
                    <div class="titleArea">
                        <div class="title"><a href="<?php echo get_permalink()?>"><?php echo get_the_title()?></a></div>
                        <div class="time"><?php echo "منذ ".$time." ".$dur?> </div>
                    </div>
                    <?php if($x % 2 == 0){
                        ?>
                        <div class="description">
                            <div class="in">
                                <p><?php echo get_the_content()?></p>
                            </div>
                        </div>
                    <?php }?>

                    <div class="shareAndSection clearfix">
                        <a href="<?= $tag_url?>" class="pullRight"><?php if($tag != null) echo $tag?></a>
                        <div class="mainSocial pullLeft">
                            <a href="#"><i class="icon-whatsapp"></i></a>
                            <a href="#"><i class="icon-twitter"></i></a>
                            <a href="#"><i class="icon-facebook-official"></i></a>
                        </div>
                    </div>
                </div>

            </div>
        </div>


<?php $x++;}?>
        </div>

    </div>
</div>
<?php }else{?>
<div class="mainContent">
    <div class="gridContainer">

        <div class="noResults">
            <p>لا توجد نتائج</p>
        </div>

    </div>
</div>
<?php }get_footer()?>

