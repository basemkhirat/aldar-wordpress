<?php

get_header();
$tag_id = get_queried_object()->term_id;
$args = array(
    'post_type' => array('main-slider','news','blog'),
    'tag__in' => $tag_id,
    'posts_per_page' => 8
);
$posts = new WP_Query($args);
var_dump($posts->post_count);
$tag = get_tag($tag_id)->name;
$url = get_tag_link($tag_id);
if($posts->have_posts()){
    $posts->the_post();
    $tag = null;
    if(has_tag()) {
        $tag = get_the_tags()[0]->name;
        $tag_url = get_tag_link(get_the_tags()[0]->term_id);
    }
    $time = get_post_diff(get_post_time("U", true, get_the_ID()));
    $dur = $time[0];
    $time = $time[1];
    $x = 1;
    ?>
    <div class="mainContent">

        <div class="gridContainer">
            <div class="myrow clearfix">

                <div class="mycol-lg-9">

                    <div class="myrow clearfix">

                        <div class="mycol-xmd-8">
                            <div class="mainPost bigPost mrgBtm">

                                <div class="avatar"><img src="<?php echo get_the_post_thumbnail_url()?>" class="bgCover" alt=""></div>
                                <div class="content">
                                    <div class="titleArea">
                                        <div class="title"><a href="<?php echo get_permalink()?>"><?php echo get_the_title()?></a></div>
                                        <div class="time"><?php echo "منذ ".$time." ".$dur?> </div>
                                    </div>
                                    <div class="description">
                                        <div class="in">
                                            <p><?php echo get_the_content()?></p>
                                        </div>
                                    </div>
                                    <div class="shareAndSection clearfix">
                                        <a href="<?= $tag_url?>" class="pullRight"><?php if($tag != null) echo $tag?></a>
                                        <div class="mainSocial pullLeft">
                                            <a href="<?php echo get_permalink()."?share=true&sharer=twitter&id".get_the_ID()?>"><i class="icon-twitter"></i></a>
                                            <a href="<?php echo get_permalink()."?share=true&sharer=facebook&id=".get_the_ID()?>"><i class="icon-facebook-official"></i></a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <?php while($posts->have_posts()){
                            $posts->the_post();
                            $time = get_post_diff(get_post_time("U", true, get_the_ID()));
                            $dur = $time[0];
                            $time = $time[1];
                            $tag = null;
                            if(has_tag())
                            {
                                $tag = get_the_tags()[0]->name;
                                $tag_url = get_tag_link(get_the_tags()[0]->term_id);
                            }
                            ?>
                            <div class="mycol-xmd-4 mycol-sm-6">
                                <div class="mainPost mrgBtm">

                                    <?php if($x % 2 != 0){
                                        ?><div class="avatar"><img src="<?php echo get_the_post_thumbnail_url()?>" alt=""></div>
                                    <?php }?>
                                    <div class="content">
                                        <div class="titleArea">
                                            <div class="title"><a href="<?php echo get_permalink()?>"><?php echo get_the_title()?></a></div>
                                            <div class="time"><?php echo "منذ ".$time." ".$dur?> </div>
                                        </div>
                                        <?php if($x % 2 == 0){
                                            ?>
                                            <div class="description">
                                                <div class="in">
                                                    <p><?php echo get_the_content()?></p>
                                                </div>
                                            </div>
                                        <?php }?>

                                        <div class="shareAndSection clearfix">
                                            <a href="<?= $tag_url?>" class="pullRight"><?php if($tag != null) echo $tag?></a>
                                            <div class="mainSocial pullLeft">
                                                <a href="<?php echo get_permalink()."?share=true&sharer=twitter&id".get_the_ID()?>"><i class="icon-twitter"></i></a>
                                                <a href="<?php echo get_permalink()."?share=true&sharer=facebook&id=".get_the_ID()?>"><i class="icon-facebook-official"></i></a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <?php $x++;}?>

                        <div class="more" style="display: none">
                            <input name="post_type" id="post_type" value="news">
                            <input name="name" id="name" value="0">
                            <input name="ppp" id="ppp" value="12">
                            <input name="cat_id" id="cat_id" value="<?php echo $tag_id?>">
                            <?php session_start(); $_SESSION['type'] = 'tag'; ?>
                        </div>
                    </div>

                    <div id="cload_more" style="cursor: pointer"> <a class="mainBtn">المزيد</a></div>

                </div>

            </div>
        </div>

    </div>
<?php } get_footer();?>