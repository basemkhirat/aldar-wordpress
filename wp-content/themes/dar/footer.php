<?php wp_footer();
?>
<footer id="footer">
    <div class="top white-bg">
        <div class="gridContainer">

            <div class="footerLogo"><img src="<?php echo get_template_directory_uri().'/assets/images/logo.png'; ?>" alt=""></div>
            <div class="footerItems clearfix">
                <div class="oneItem ipadHide">
                    <div class="singleTitle underline mrgBtm">الدار</div>
                    <ul class="footerMenu clearfix">
                        <?php wp_nav_menu(['menu'   =>'footer2']);?>
                    </ul>
                </div>
                <div class="oneItem ipadHide">
                    <div class="singleTitle underline mrgBtm">المحتوي</div>
                    <ul class="footerMenu clearfix">
                        <?php wp_nav_menu('footer');?>
                    </ul>
                </div>
                <div class="oneItem">
                    <div  id="send">
                    <div class="singleTitle underline mrgBtm">تواصل معنا</div>
                    <div class="subscribeForm mrgBtm">
                        <form action="#">
                            <input type="text" class="formItem" id="nemail" placeholder="البريد الالكتروني">
                            <button type="submit" id="newsletter" class="submit"><i class="icon-arrowleft"></i></button>
                        </form>
                        <p class="f-12 medium white" style="color: #c93034 " id="invalid"></p>
                    </div>
                    </div>
                        <div id="sendNews"></div>
                    <?php
                    $facebook = get_post_meta(589,'facebook',true);
                    $linkedin = get_post_meta(589,'linkedin',true);
                    $instagram = get_post_meta(589,'instagram',true);
                    $twitter = get_post_meta(589,'twitter',true);
                    $google = get_post_meta(589,'google plus',true);
                    ?>
                    <div class="mainSocial">
                        <a href="<?php echo $linkedin?>" target="_blank"><i class="icon-linkedin"></i></a>
                        <a href="<?php echo $google?>" target="_blank"><i class="icon-gplus"></i></a>
                        <a href="<?php echo $instagram?>" target="_blank"><i class="icon-instagram"></i></a>
                        <a href="<?php echo $twitter?>" target="_blank"><i class="icon-twitter"></i></a>
                        <a href="<?php echo $facebook?>" target="_blank"><i class="icon-facebook-official"></i></a>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="brand-bg mainPadding">
        <div class="gridContainer clearfix">
            <div class="pullRight font-2 white-color">جميع الحقوق محفوظه © 2018</div>
            <div class="pullLeft font-2 white-color-links mobileHide">
                <a href="<?php echo site_url().'/privacy'?>">سياسه الخصوصيه</a>
                <span class="circleSperator"></span>
                <a href="<?php echo site_url().'/terms'?>">الشروط والاحكام</a>
            </div>
        </div>
    </div>
</footer>

<script>
    $(document).on('click','#newsletter',function(e){
        e.preventDefault();
        email =  document.getElementById('nemail').value;
        $('#load').remove();
        var button = $(this),
            data = {
                'action': 'news_letter',
                'email':email
            };
        $.ajax({
            url : load_more.ajaxurl, // AJAX handler
            data : data,
            type : 'POST',
            success : function( data ){
                if( data[0].success != null  ) {
                    $('#send').fadeOut(2000,function () {
                        $("<p class=\"sectionTitle\" style='font-size: 24px;" + "color: #C73133;" + "font-family: \"bold\";'>"+data[0].success+"</p>").insertBefore('#sendNews');
                    });
                }else{
                    document.getElementById('invalid').innerHTML = data[0].error;
                }
            }
        });
    });
</script>